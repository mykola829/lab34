import 'package:adityagurjar/widgets/responsive_widget.dart';
import 'package:flutter/material.dart';

class LogInPage extends StatefulWidget {
  static const route = "/loginPage";
  final String title;
  LogInPage({Key key, this.title}) : super(key: key);

  @override
  _LogInPageState createState() => new _LogInPageState();
}

enum FormType { login, register }

class _LogInPageState extends State<LogInPage> {
  static final formKey = new GlobalKey<FormState>();

  String _email;
  String _password;
  String _username;
  String _confirmedPassword;
  String _hintText = '';
  FormType _formType = FormType.login;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('LogIn', textAlign: TextAlign.center,),
      ),
      body: body(context),
    );
  }

  Widget body(BuildContext context) {
    if (ResponsiveWidget.isSmallScreen(context)) {
      return smallScreen(context);
    }

    return largeScreen(context);
  }

  Widget smallScreen(BuildContext context) {
    return SingleChildScrollView(
          child: Container(
          padding: const EdgeInsets.all(20.0),
          child: Column(children: [
            Card(
                child:
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Container(
                  padding: const EdgeInsets.all(20.0),
                  child: Form(
                      key: formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: (_formType == FormType.login
                                ? loginForm()
                                : signUpForm()) +
                            [
                              SizedBox(
                                height: 30,
                              )
                            ] +
                            submissionOptions(), // adding two widget lists
                      ))),
            ])),
            hintText(),
          ])),
    );
  }

  Widget largeScreen(BuildContext context) {
    return SingleChildScrollView(
          child: Center(
      child: Container(
      width: MediaQuery.of(context).size.width / 2,
      child: Container(
          padding: const EdgeInsets.all(20.0),
          child: Column(children: [
            Card(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
      Container(
          padding: const EdgeInsets.all(20.0),
          child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment:
                    CrossAxisAlignment.stretch,
                children: (_formType == FormType.login
                        ? loginForm()
                        : signUpForm()) +
                    [
                      SizedBox(
                        height: 30,
                      )
                    ] +
                    submissionOptions(), // adding two widget lists
              ))),
          ])),
            hintText(),
          ]))),
          ),
    );
  }

  void submitForm() async {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();

      //hides keyboard
      FocusScope.of(context).requestFocus(new FocusNode());
      try {} catch (e) {
        setState(() {
          _hintText = 'Sign In Error\n\n${e.message}';
        });
      }
    } else {
      setState(() {
        _hintText = '';
      });
    }
  }

  void register() {
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.register;
      _hintText = '';
    });
  }

  void signIn() {
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.login;
      _hintText = '';
    });
  }

  List<Widget> signUpForm() {
    return [
      padded(
          child: TextFormField(
        key: Key('username'),
        decoration: InputDecoration(labelText: 'Username'),
        autocorrect: false,
        validator: (val) => val.isEmpty ? 'Username can\'t be empty.' : null,
        onSaved: (val) => _email = val,
      )),
      padded(
          child: TextFormField(
        key: Key('email'),
        decoration: InputDecoration(labelText: 'Email'),
        autocorrect: false,
        validator: (val) => val.isEmpty ? 'Email can\'t be empty.' : null,
        onSaved: (val) => _email = val,
      )),
      padded(
          child: TextFormField(
        key: Key('fullName'),
        decoration: InputDecoration(labelText: 'Full name'),
        autocorrect: false,
        validator: (val) => val.isEmpty ? 'Full name can\'t be empty.' : null,
        onSaved: (val) => _email = val,
      )),
      padded(
          child: TextFormField(
        key: Key('password'),
        decoration: InputDecoration(labelText: 'Password'),
        obscureText: true,
        autocorrect: false,
        validator: (val) => val.isEmpty ? 'Password can\'t be empty.' : null,
        onSaved: (val) => _password = val,
      )),
      padded(
          child: TextFormField(
        key: Key('confirmPassword'),
        decoration: InputDecoration(labelText: 'Confirm password'),
        obscureText: true,
        autocorrect: false,
        validator: (val) => val.isEmpty ? 'Password can\'t be empty.' : null,
        onSaved: (val) => _password = val,
      )),
    ];
  }

  List<Widget> loginForm() {
    return [
      padded(
          child: TextFormField(
        key: Key('username'),
        decoration: InputDecoration(labelText: 'Username'),
        autocorrect: false,
        validator: (val) => val.isEmpty ? 'Username can\'t be empty.' : null,
        onSaved: (val) => _email = val,
      )),
      padded(
          child: TextFormField(
        key: Key('password'),
        decoration: InputDecoration(labelText: 'Password'),
        obscureText: true,
        autocorrect: false,
        validator: (val) => val.isEmpty ? 'Password can\'t be empty.' : null,
        onSaved: (val) => _password = val,
      )),
    ];
  }

  List<Widget> submissionOptions() {
    switch (_formType) {
      case FormType.login:
        return [
          MaterialButton(
              key: Key('login'),
              child: Text('Login'),
              height: 44.0,
              color: Colors.blue,
              onPressed: submitForm),
          SizedBox(
            height: 10,
          ),
          FlatButton(
              key: Key('signUp'),
              child: Text(
                "Need an account? Sign Up",
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
              onPressed: register),
        ];
      case FormType.register:
        return [
          MaterialButton(
              key: Key('create_account'),
              child: Text('Sign Up'),
              height: 44.0,
              color: Colors.blue,
              onPressed: submitForm),
          SizedBox(
            height: 10,
          ),
          FlatButton(
              key: Key('sign_in'),
              child: Text(
                "Registered already ? Login",
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
              onPressed: signIn),
        ];
    }
    return null;
  }

  Widget hintText() {
    return Container(
        padding: const EdgeInsets.all(32.0),
        child: new Text(_hintText,
            key: Key('hint_text'),
            style: TextStyle(fontSize: 18.0, color: Colors.grey),
            textAlign: TextAlign.center));
  }

  Widget padded({Widget child}) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0),
      child: child,
    );
  }
}
