import 'package:adityagurjar/models/news_model.dart';
import 'package:adityagurjar/widgets/responsive_widget.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'dart:html' as html;

class NewsDetail extends StatelessWidget {
  final News news;
  bool isCommented;
  bool isLiked;
  NewsDetail(
      {Key key,
      @required this.news,
      @required this.isCommented,
      @required this.isLiked,
      String id})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              title: Text(news.title),
              elevation: 0.0,
            ),
            body: ResponsiveWidget(
              largeScreen: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Container(),
                  ),
                  Expanded(
                    flex: 2,
                    child: blogList(context),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(),
                  )
                ],
              ),
              smallScreen: blogList(context),
            )));
  }

  Widget blogList(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.network(
              news.urlToImage,
            ),
            SizedBox(
              height: 30.0,
            ),
            Text(
              "Author: ${news.author}",
            ),
            SizedBox(
              height: 30.0,
            ),
            Text(
              "${news.content}",
              style: TextStyle(
                fontSize: 22,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: 'See more ',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 22,
                    ),
                  ),
                  TextSpan(
                    text: 'here',
                    style: TextStyle(
                      color: Colors.blue,
                      fontSize: 22,
                    ),
                    recognizer: new TapGestureRecognizer()
                      ..onTap = () {
                        if (news.url != null)
                          html.window.open(news.url, 'adityadroid');
                      },
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                _ShowStat(
                  icon: GestureDetector(
                    child: !isLiked
                        ? Icon(Icons.favorite_border, color: Colors.red)
                        : Icon(Icons.favorite, color: Colors.red),
                    onTap: () {
                      Navigator.of(context).pop();
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => NewsDetail(
                            news: news,
                            isCommented: isCommented,
                            isLiked: !isLiked,
                          ),
                        ),
                      );
                    },
                  ),
                  number: 4,
                  color: Colors.red,
                ),
                _ShowStat(
                  icon: GestureDetector(
                    child: Icon(
                      Icons.remove_red_eye,
                      color: Colors.blue,
                    ),
                    onTap: () {},
                  ),
                  number: 5,
                  color: Colors.green[900],
                ),
                _ShowStat(
                  icon: GestureDetector(
                    child: Icon(
                      Icons.edit,
                    ),
                    onTap: () {
                      Navigator.of(context).pop();
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => NewsDetail(
                            news: news,
                            isCommented: !isCommented,
                            isLiked: isLiked,
                          ),
                        ),
                      );
                    },
                  ),
                  number: 5,
                  color: Colors.green[900],
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            isCommented
                ? TextField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        suffixIcon: IconButton(
                            icon: Icon(Icons.touch_app),
                            onPressed: () {
                              Navigator.of(context).pop();
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => NewsDetail(
                                    news: news,
                                    isCommented: !isCommented,
                                    isLiked: isLiked,
                                  ),
                                ),
                              );
                            })),
                    maxLength: 250,
                  )
                : Container(),
            ExpansionTile(
              leading: Icon(Icons.comment),
              trailing: Row(mainAxisSize: MainAxisSize.min, children: [
                Text("9"),
              ]),
              title: Text("Comments"),
              children: List<Widget>.generate(
                  9,
                  (int index) => ListTile(
                        leading: CircleAvatar(
                          backgroundImage: NetworkImage(
                              "https://i.stack.imgur.com/Dw6f7.png"),
                        ),
                        subtitle: Text(
                            'Comes with humpsComes with humpsComes with humpsComes with humpsComes with humpsComes with humpsComes with humpsComes with humpsComes with humpsComes with humpsComes with humpsComes with humpsComes with humpsComes with humpsComes with humpsComes with humps'),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            _ShowStat(
                              icon: GestureDetector(
                                child: Icon(
                                  Icons.thumb_up,
                                ),
                                onTap: () {},
                              ),
                              number: 5,
                              color: Colors.green[900],
                            ),
                            SizedBox(width: 5),
                            _ShowStat(
                              icon: GestureDetector(
                                child: Icon(
                                  Icons.thumb_down,
                                ),
                                onTap: () {},
                              ),
                              number: 5,
                              color: Colors.green[900],
                            ),
                          ],
                        ),
                        title: Text('Horse'),
                      )),
            )
          ],
        ),
      ),
    );
  }
}

class _ShowStat extends StatelessWidget {
  final GestureDetector icon;
  final int number;
  final Color color;

  const _ShowStat({
    Key key,
    @required this.icon,
    @required this.number,
    @required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 2.0),
          child: icon,
        ),
        Text(number.toString()),
      ],
    );
  }
}
