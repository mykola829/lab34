import 'package:adityagurjar/config/assets.dart';
import 'package:adityagurjar/tabs/home_tab.dart';
import 'package:adityagurjar/tabs/news_tab.dart';
import 'package:adityagurjar/tabs/projects_tab.dart';
import 'package:adityagurjar/widgets/theme_inherited_widget.dart';
import 'package:flutter/material.dart';

import 'login_page.dart';

class HomePage extends StatefulWidget {
  static const route = "/homePage";
  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  static String categoryAPI = '';
  static List<Widget> tabWidgets = <Widget>[
    
    NewsTab(),
    HomeTab(),
    ProjectsTab(),
  ];

  @override
  void initState() {
    super.initState();
    
  }

  

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 6,
      child: Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                'Email: mykola829@gmail.com',
                style: TextStyle(fontSize: 16),
              ),
              Text(
                'Username: mykola829',
                style: TextStyle(fontSize: 16),
              ),
              Text(
                'Full name: Mykola Romanyshyn',
                style: TextStyle(fontSize: 16),
              ),
            ],
          ),
        
          actions: <Widget>[
            MaterialButton(
              padding: const EdgeInsets.only(right: 5),
              onPressed: () {
                Navigator.of(context).pushNamed(LogInPage.route);
              },
              child: Text(
                'Login',
                style: TextStyle(fontSize: 16),
              ),
            ),
            IconButton(
              icon: ThemeSwitcher.of(context).isDarkModeOn
                  ? Icon(Icons.wb_sunny)
                  : Image.asset(
                      Assets.moon,
                      height: 20,
                      width: 20,
                    ),
              onPressed: () => ThemeSwitcher.of(context).switchDarkMode(),
            )
          ],
        ),
        body: Center(
          child: tabWidgets.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.chrome_reader_mode),
              title: Text('Home'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.category),
              title: Text('Categories'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.show_chart),
              title: Text('Activity'),
            )
          ],
          currentIndex: _selectedIndex,
          onTap: (index) => setState(() => _selectedIndex = index),
          selectedItemColor: Theme.of(context).accentColor,
        ),
      ),
    );
  }
}
