import 'package:adityagurjar/pages/home_page.dart';
import 'package:adityagurjar/pages/login_page.dart';
import 'package:adityagurjar/widgets/theme_inherited_widget.dart';
import 'package:flutter/material.dart';

import 'config/themes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ThemeSwitcherWidget(
      initialDarkModeOn: false,
      child: RootPage(),
    );
  }
}
class RootPage extends StatelessWidget {
  const RootPage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Mykola Romanyshyn',
      theme:ThemeSwitcher.of(context).isDarkModeOn?darkTheme(context):lightTheme(context),
      initialRoute: HomePage.route,
      routes: {LogInPage.route: (context) => LogInPage(),
      HomePage.route: (context) => HomePage()},
    );
  }
}
