class News {
  String id;
  String name;
  String author;
  String title;
  String description;
  String url;
  String urlToImage;
  String publishedAt;
  String content;
  int countOfLike = 1500;
Map<String, dynamic> sources;

  News(
      {this.id,
      this.name,
      this.author,
      this.title,
      this.description,
      this.url,
      this.urlToImage,
      this.publishedAt,
      this.content,
        });

  News.fromJson(Map<String, dynamic> json) {
     sources = json['source'];
     name = sources['name'];
     id = sources['id'];
     author = json['author'];
     title  = json['title'];
     description = json['description'];
     url = json['url'];
     urlToImage  = json['urlToImage'];
     publishedAt = json['publishedAt'];
     content  = json['content'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    
    return data;
  }
}

