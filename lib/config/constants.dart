class Constants{
  static const String NEWS_API = 'https://newsapi.org/v2/top-headlines?country=us&apiKey=49fe216f14c8482793f79ec3d4a49a2a';
  static const MEDIUM_IMAGE_CDN = 'https://miro.medium.com/fit/c/700/300/';
  static const BLOG_URL = 'http://www.medium.com/@adityadroid/';
  static const PROFILE_FACEBOOK = 'https://www.facebook.com/mykola.romanyshyn.1/';
  static const PROFILE_GITHUB = 'https://github.com/mykola829/';
  static const PROFILE_INSTAGRAM = 'https://www.instagram.com/mykola.romanyshyn/';
}