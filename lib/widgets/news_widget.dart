import 'package:adityagurjar/pages/newsDetail.dart';
import 'package:flutter/material.dart';

class NewsWidget extends StatelessWidget {
  final news;
  final index;
  final length;
  bool isLiked = false;

  NewsWidget(this.news, this.index, this.length, this.isLiked);
  @override
  Widget build(BuildContext context) {
    double topBottomPadding = (index == 0 || index == length - 1) ? 16.0 : 8.0;
    return InkWell(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => NewsDetail(news: news, isCommented: false,isLiked: false),
        ),
      ),
      child: Card(
        margin:
            EdgeInsets.fromLTRB(16.0, topBottomPadding, 16.0, topBottomPadding),
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              news.urlToImage != null
                  ? Image.network(news.urlToImage)
                  : Container(),
              SizedBox(
                height: 16,
              ),

              SizedBox(
                height: 8,
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      news.name,
                      style: Theme.of(context).textTheme.title,
                    ),
                    Row(children: <Widget>[
                      GestureDetector(
                        child: isLiked
                            ? Icon(Icons.favorite, color: Colors.red)
                            : Icon(
                                Icons.favorite_border,
                                color: Colors.red,
                              ),
                        onTap: () {},
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(news.countOfLike.toString(),
                          style: Theme.of(context).textTheme.title)
                    ])
                  ]),
              Text(news.author != null ? news.author : '', style: Theme.of(context).textTheme.subtitle),
              SizedBox(
                width: 100,
              ),

              //Text(blog.virtuals.totalClapCount)
            ],
          ),
        ),
      ),
    );
  }
}
