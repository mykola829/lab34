import 'package:adityagurjar/config/constants.dart';
import 'package:adityagurjar/models/news_model.dart';
import 'package:adityagurjar/providers/api_provider.dart';
import 'package:adityagurjar/widgets/news_widget.dart';
import 'package:adityagurjar/widgets/responsive_widget.dart';
import 'package:flutter/material.dart';

class NewsTab extends StatefulWidget {
 
  @override
  _NewsTabState createState() => _NewsTabState();

}

class _NewsTabState extends State<NewsTab> {
  ApiProvider _apiProvider = ApiProvider();
  bool _loadingData = true;
  bool _showError = false;
     List<News> _news = [];

  @override
  void initState() {
    super.initState();
    _news.clear();
    loadBlogs();

  }
 
  @override
  Widget build(BuildContext context) {
    if (_loadingData)
      return Center(
        child: CircularProgressIndicator(),
      );
    if (_showError) {
      Center(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Something went wrong',
              style: Theme.of(context).textTheme.headline,
            ),
          ),
          RaisedButton(
            child: Text(
              'Retry',
              style: Theme.of(context)
                  .textTheme
                  .button
                  .copyWith(color: Colors.white),
            ),
            elevation: 0.0,
            onPressed: loadBlogs,
          )
        ],
      ));
    }
    return ResponsiveWidget(
      largeScreen: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(),
          ),
          Expanded(
            flex: 2,
            child: blogList(),
          ),
          Expanded(
            flex: 1,
            child: Container(),
          )
        ],
      ),
      smallScreen: blogList(),
    );
  }

  Widget blogList() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListView.builder(
                itemCount: _news.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) =>
                    NewsWidget(_news[index], index,_news.length,false)),
          ],
        ),
      ),
    );
  }

  void loadBlogs() async {
    setState(() {
      _loadingData = true;
      _showError = false;
    });
    final result = await _apiProvider.getBlogs(Constants.NEWS_API);
    setState(() {
      if (result == null) {
        _showError = true;
        _loadingData = false;
      } else {
        _news = result;
        _showError = false;
        _loadingData = false;
      }
    });
  }
}
