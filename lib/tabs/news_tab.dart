import 'package:adityagurjar/config/constants.dart';
import 'package:adityagurjar/models/news_info_model.dart';
import 'package:adityagurjar/models/news_model.dart';
import 'package:adityagurjar/providers/api_provider.dart';
import 'package:adityagurjar/widgets/news_widget.dart';
import 'package:adityagurjar/widgets/responsive_widget.dart';
import 'package:flutter/material.dart';

class HomeTab extends StatefulWidget {
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  ApiProvider _apiProvider = ApiProvider();
  bool _loadingData = true;
  bool _showError = false;
  String categoryAPI;
  List<News> _news = [];
  List<NewsInfoModel> _newsModelInfo = [];
  @override
  void initState() {
    super.initState();
        _news.clear();
    _newsModelInfo.add(new NewsInfoModel(countOfView: 4,countOflike: 5,isLiked: false,id:"dsadas",));
    _newsModelInfo.add(new NewsInfoModel(countOfView: 4,countOflike: 5,isLiked: false,id:"aaasaa",));
    _newsModelInfo.add(new NewsInfoModel(countOfView: 4,countOflike: 5,isLiked: false,id:"dsaasasdas",));
    _newsModelInfo.add(new NewsInfoModel(countOfView: 4,countOflike: 5,isLiked: false,id:"asawawdwa",));
    _newsModelInfo.add(new NewsInfoModel(countOfView: 4,countOflike: 5,isLiked: false,id:"dsaawdwadas",));

    categoryAPI =
        'http://newsapi.org/v2/everything?q=business&from=2020-05-01&sortBy=publishedAt&apiKey=49fe216f14c8482793f79ec3d4a49a2a';
    loadBlogs(categoryAPI);
  }

  void changeCategory(int index) {
    String category;
    switch (index) {
      case 0:
        category = "Business";
        break;
      case 1:
        category = "Entartainment";

        break;
      case 2:
        category = "Health";

        break;
      case 3:
        category = "Science";

        break;
      case 4:
        category = "Sport";

        break;
      case 5:
        category = "Technology";

        break;
      default:
    }
    categoryAPI =
        'http://newsapi.org/v2/everything?q={$category}&from=2020-05-01&sortBy=publishedAt&apiKey=49fe216f14c8482793f79ec3d4a49a2a';
    _news.clear();
    
    loadBlogs(categoryAPI);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 6,
        child: Scaffold(
          appBar: AppBar(
              bottom: TabBar(
            onTap: (index) => setState(() => changeCategory(index)),
            isScrollable: true,
            tabs: [
              Tab(
                text: 'Business',
              ),
              Tab(
                text: 'Entartainment',
              ),
              Tab(
                text: 'Health',
              ),
              Tab(
                text: 'Science',
              ),
              Tab(
                text: 'Sport',
              ),
              Tab(
                text: 'Technology',
              ),
            ],
          )),
          body: _loadingData
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : _showError
                  ? Center(
                      child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Something went wrong',
                            style: Theme.of(context).textTheme.headline,
                          ),
                        ),
                        RaisedButton(
                          child: Text(
                            'Retry',
                            style: Theme.of(context)
                                .textTheme
                                .button
                                .copyWith(color: Colors.white),
                          ),
                          elevation: 0.0,
                          onPressed: () => loadBlogs(Constants.NEWS_API),
                        )
                      ],
                    ))
                  : ResponsiveWidget(
                      largeScreen: Row(
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Container(),
                          ),
                          Expanded(
                            flex: 2,
                            child: blogList(),
                          ),
                          Expanded(
                            flex: 1,
                            child: Container(),
                          )
                        ],
                      ),
                      smallScreen: blogList(),
                    ),
        ));
  }

  Widget blogList() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListView.builder(
                itemCount: _news.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) =>
                    NewsWidget(_news[index], index, _news.length, false)),
          ],
        ),
      ),
    );
  }

  void loadBlogs(String url) async {
    setState(() {
      _loadingData = true;
      _showError = false;
    });
    final result = await _apiProvider.getBlogs(url);
    setState(() {
      if (result == null) {
        _showError = true;
        _loadingData = false;
      } else {
        _news = result;
        _showError = false;
        _loadingData = false;
      }
    });
  }
}
